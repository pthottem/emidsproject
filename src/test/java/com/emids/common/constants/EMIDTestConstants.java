package test.java.com.emids.common.constants;

/**
 * Constants for EMIDS
 * @author PT
 *
 */
public interface EMIDTestConstants {
 
	public static final String YES = "Yes";
	public static final String NO = "No";
 	public static final int AGE = 34;
	public static final String PM_INPUT_NAME = "Norman Gomes";
	public static final String PM_INPUT_GENDER = "Male";
	public static final int PM_INPUT_AGE = 34;


}