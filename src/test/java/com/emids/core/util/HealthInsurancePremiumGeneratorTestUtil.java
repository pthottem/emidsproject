package test.java.com.emids.core.util;


import test.java.com.emids.common.constants.EMIDTestConstants;
import main.java.com.emids.common.constants.EMIDConstants;
import main.java.com.emids.common.constants.Habits;
import main.java.com.emids.common.constants.Health;
import main.java.com.emids.core.model.HealthInputVO;


/**
 * class to build test data
 * @author PT
 * @dte 2/8/2018
 *
 */


public class HealthInsurancePremiumGeneratorTestUtil{
	   /**
	    * Method : getHabitsObj
	    * set habits test data 
	    * @return
	    */
	 	public static Habits getHabitsObj(){
	 		
	 		Habits habitsObj = new Habits();
	 		habitsObj.setAlcohol(EMIDConstants.YES);
	 		habitsObj.setDailyexercise(EMIDConstants.YES);
	 		habitsObj.setDrugs(EMIDConstants.NO);
	 		habitsObj.setSmoking(EMIDConstants.NO);
	 		
			return habitsObj;		
	}
	 	/**
	 	 * Method : getHealthObj
	 	 * set health test data
	 	 * @return
	 	 */
	 	public static  Health getHealthObj(){
	 	    
	 		Health healthObj =  new Health();
	 		healthObj.setBloodpressure(EMIDConstants.NO);
	 		healthObj.setBloodsugar(EMIDConstants.NO);
	 		healthObj.setHypertension(EMIDConstants.NO);
	 		healthObj.setOverweight(EMIDConstants.YES);
	 	 

			return healthObj;		
	}
	 	/**
	 	 * Build input vo for health premium
	 	 * @return
	 	 */
	 	public static HealthInputVO buildInputObj(){
	 		
	 		HealthInputVO healthInputVOObj  = new HealthInputVO();
	 		healthInputVOObj.setAge(EMIDTestConstants.PM_INPUT_AGE);
	 		healthInputVOObj.setGender(EMIDTestConstants.PM_INPUT_GENDER);
	 		healthInputVOObj.setName(EMIDTestConstants.PM_INPUT_NAME);
	 		healthInputVOObj.setHabits(getHabitsObj());
	 		healthInputVOObj.setHealth(getHealthObj());
	 		
	 		return healthInputVOObj;
	 	}

	
	
}