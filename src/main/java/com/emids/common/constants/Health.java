package main.java.com.emids.common.constants;


/**
 * Define health entity
 * @author PT
 * @dte 2/8/2018
 *
 */
public class Health{

	private String bloodpressure;
	private String overweight;
	private String bloodsugar;
	private String hypertension;
	public String getBloodpressure() {
		return bloodpressure;
	}
	public void setBloodpressure(String bloodpressure) {
		this.bloodpressure = bloodpressure;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public String getBloodsugar() {
		return bloodsugar;
	}
	public void setBloodsugar(String bloodsugar) {
		this.bloodsugar = bloodsugar;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bloodpressure == null) ? 0 : bloodpressure.hashCode());
		result = prime * result
				+ ((bloodsugar == null) ? 0 : bloodsugar.hashCode());
		result = prime * result
				+ ((hypertension == null) ? 0 : hypertension.hashCode());
		result = prime * result
				+ ((overweight == null) ? 0 : overweight.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Health other = (Health) obj;
		if (bloodpressure == null) {
			if (other.bloodpressure != null)
				return false;
		} else if (!bloodpressure.equals(other.bloodpressure))
			return false;
		if (bloodsugar == null) {
			if (other.bloodsugar != null)
				return false;
		} else if (!bloodsugar.equals(other.bloodsugar))
			return false;
		if (hypertension == null) {
			if (other.hypertension != null)
				return false;
		} else if (!hypertension.equals(other.hypertension))
			return false;
		if (overweight == null) {
			if (other.overweight != null)
				return false;
		} else if (!overweight.equals(other.overweight))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Health [bloodpressure=" + bloodpressure + ", overweight="
				+ overweight + ", bloodsugar=" + bloodsugar + ", hypertension="
				+ hypertension + "]";
	}
	
	

}