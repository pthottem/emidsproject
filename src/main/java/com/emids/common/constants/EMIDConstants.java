package main.java.com.emids.common.constants;
/**
 * Constants for EMIDS
 * @author PT
 *
 */
public interface EMIDConstants {
 
	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String GENDER_OTHER = "Other";
	public static final String GENDER_MALE = "Male";
	public static final String GENDER_FEMALE = "Female";
	public static final String PREMIUM_STR = "Health Insurance Premium for ";

}