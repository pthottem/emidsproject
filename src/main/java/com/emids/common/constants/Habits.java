package main.java.com.emids.common.constants;

/**
 * Define habits entity
 * 
 * @author PT
 * @dte 2/8/2018
 *
 */
public class Habits {

	private String smoking;
	private String alcohol;
	private String dailyexercise;
	private String drugs;
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyexercise() {
		return dailyexercise;
	}
	public void setDailyexercise(String dailyexercise) {
		this.dailyexercise = dailyexercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	@Override
	public String toString() {
		return "Habits [smoking=" + smoking + ", alcohol=" + alcohol
				+ ", dailyexercise=" + dailyexercise + ", drugs=" + drugs + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alcohol == null) ? 0 : alcohol.hashCode());
		result = prime * result
				+ ((dailyexercise == null) ? 0 : dailyexercise.hashCode());
		result = prime * result + ((drugs == null) ? 0 : drugs.hashCode());
		result = prime * result + ((smoking == null) ? 0 : smoking.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Habits other = (Habits) obj;
		if (alcohol == null) {
			if (other.alcohol != null)
				return false;
		} else if (!alcohol.equals(other.alcohol))
			return false;
		if (dailyexercise == null) {
			if (other.dailyexercise != null)
				return false;
		} else if (!dailyexercise.equals(other.dailyexercise))
			return false;
		if (drugs == null) {
			if (other.drugs != null)
				return false;
		} else if (!drugs.equals(other.drugs))
			return false;
		if (smoking == null) {
			if (other.smoking != null)
				return false;
		} else if (!smoking.equals(other.smoking))
			return false;
		return true;
	}
    
	
}