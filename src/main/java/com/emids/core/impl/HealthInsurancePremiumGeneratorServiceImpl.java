package main.java.com.emids.core.impl;

import java.text.DecimalFormat;

import main.java.com.emids.common.constants.EMIDConstants;
import main.java.com.emids.common.constants.Habits;
import main.java.com.emids.common.constants.Health;
import main.java.com.emids.core.model.HealthInputVO;

/**
 * class to generate health insurance premium of a person
 * 
 * @author PT
 * @dte 2/8/2018
 *
 */

public class HealthInsurancePremiumGeneratorServiceImpl {
	private Double basePremium = new Double(5000);

	/**
	 * Method name : getPremiumQuote
	 */
	public Double calculateHealthPremium(HealthInputVO healthInputVOObj) {
		try {
			System.out.println("Enter method calculateHealthPremium");

			genderFilter(healthInputVOObj.getGender());
			ageFilter(healthInputVOObj.getAge());
			healthFilter(healthInputVOObj.getHealth());
			habitFilter(healthInputVOObj.getHabits());

			System.out.println("End method calculateHealthPremium");

		} catch (Exception e) {

		}
		return Double.valueOf(new DecimalFormat("0").format(basePremium));

	}

	/**
	 * Method: ageFilter Purpose : calculate premium based on age
	 * 
	 * @param age
	 */
	private void ageFilter(int age) {

		if (age >= 18 && age < 25) {

			basePremium += (basePremium * 10 / 100);

		}

		if (age >= 25 && age < 30) {

			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);

		}
		if (age >= 30 && age < 35) {

			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);

		}
		if (age >= 35 && age < 40) {

			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);

		}
		if (age >= 40) {
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 10 / 100);
			basePremium += (basePremium * 20 / 100);

		}

		System.out.println("premium after applying age filter  = "
				+ basePremium);

	}

	/**
	 * Method :genderFilter Purpose: calculate premium based on gender
	 * 
	 * @param gender
	 */
	private void genderFilter(String gender) {

		if (gender.equalsIgnoreCase(EMIDConstants.GENDER_MALE)) {

			basePremium += (basePremium * 2 / 100);

		}
		System.out.println("premium after applying gender filter = "
				+ basePremium);

	}

	/**
	 * Method: habitFilter Purpose: calculates premium based on habits
	 * 
	 * @param habitsObj
	 */
	private void habitFilter(Habits habitsObj) {
		if (habitsObj.getDailyexercise().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium -= (basePremium * 3 / 100);

		} else {

			basePremium += (basePremium * 1 / 100);

		}
		if (habitsObj.getAlcohol().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium += (basePremium * 1 / 100);

		}
		if (habitsObj.getDrugs().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium += (basePremium * 1 / 100);

		}
		if (habitsObj.getSmoking().equalsIgnoreCase(EMIDConstants.YES)) {
			basePremium += (basePremium * 1 / 100);

		}
		System.out.println("premium after applying habits filter = "
				+ basePremium);

	}

	/**
	 * Method: healthFilter Purpose: calculates premium based on health
	 * 
	 * @param healthObj
	 */
	private void healthFilter(Health healthObj) {

		if (healthObj.getBloodpressure().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium += (basePremium * 1 / 100);

		}
		if (healthObj.getBloodsugar().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium += (basePremium * 1 / 100);

		}
		if (healthObj.getHypertension().equalsIgnoreCase(EMIDConstants.YES)) {

			basePremium += (basePremium * 1 / 100);

		}
		if (healthObj.getOverweight().equalsIgnoreCase(EMIDConstants.YES)) {
			basePremium += (basePremium * 1 / 100);

		}
		System.out.println("premium after applying health filter = "
				+ basePremium);

	}

}
