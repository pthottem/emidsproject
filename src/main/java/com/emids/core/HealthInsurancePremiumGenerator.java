package main.java.com.emids.core;

import main.java.com.emids.common.constants.EMIDConstants;
import main.java.com.emids.core.impl.HealthInsurancePremiumGeneratorServiceImpl;
import main.java.com.emids.core.model.HealthInputVO;

/**
 * class to generate health insurance premium of a person
 * @author PT
 * @dte 2/7/2018
 *
 */

public class HealthInsurancePremiumGenerator{
	
	/**
	 * Method name : getPremiumQuote
	 * Purpose : returns health insurance quote of a person
	 * @throws Exception 
	 * @healthInputVO
	 * return
	 */
	public void getPremiumQuote(HealthInputVO healthInputVO) throws Exception{
		try{
			System.out.println("Enter method getPremiumQuote");
			Double premiumValue = new HealthInsurancePremiumGeneratorServiceImpl()
							.calculateHealthPremium(healthInputVO);

			System.out.println(EMIDConstants.PREMIUM_STR
					.concat(healthInputVO.getName())
					.concat(" : ")
					.concat(String.valueOf(premiumValue.longValue())));
			System.out.println("End method getPremiumQuote");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
 }
